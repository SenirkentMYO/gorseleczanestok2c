﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselEczaneStok2C
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 x = new Form1();
            this.Hide();
            x.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> satıs = database.İlacAlım.Where(a => a.İlacAdi.Contains(textBox5.Text)).ToList();
            dataGridView1.DataSource = satıs;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> satış = database.İlacAlım.ToList();
            dataGridView1.DataSource = satış;
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> satış = database.İlacAlım.ToList();
            dataGridView1.DataSource = satış;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox7.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            // textBox6.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
