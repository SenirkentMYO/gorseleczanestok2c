﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselEczaneStok2C
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form1 x = new Form1();
            this.Hide();
            x.ShowDialog();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> depo = database.İlacAlım.Where(a => a.İlacAdi.Contains(textBox1.Text)).ToList();
            dataGridView1.DataSource = depo;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> depo = database.İlacAlım.ToList();
            dataGridView1.DataSource = depo;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ECZANEEntities1 database = new ECZANEEntities1();
            List<İlacAlım> depo = database.İlacAlım.ToList();
            dataGridView1.DataSource = depo;
            textBox1.Clear();
        }
    }
}
