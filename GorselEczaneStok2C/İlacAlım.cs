//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GorselEczaneStok2C
{
    using System;
    using System.Collections.Generic;
    
    public partial class İlacAlım
    {
        public int ID { get; set; }
        public string İlacAdi { get; set; }
        public string FirmaAdi { get; set; }
        public string DepoAdi { get; set; }
        public Nullable<int> Fiyat { get; set; }
        public Nullable<int> Stok { get; set; }
    }
}
