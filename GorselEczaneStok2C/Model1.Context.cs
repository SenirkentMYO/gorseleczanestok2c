﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GorselEczaneStok2C
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ECZANEEntities1 : DbContext
    {
        public ECZANEEntities1()
            : base("name=ECZANEEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<DepoAdi> DepoAdi { get; set; }
        public DbSet<Eczanem> Eczanem { get; set; }
        public DbSet<FirmaAdi> FirmaAdi { get; set; }
        public DbSet<İlacAdi> İlacAdi { get; set; }
        public DbSet<İlacAlım> İlacAlım { get; set; }
        public DbSet<satis> satis { get; set; }
    }
}
